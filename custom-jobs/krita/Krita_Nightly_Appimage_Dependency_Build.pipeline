// Request a node to be allocated to us
node( "Appimage1604" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/graphics/krita.git']]
			]

		}

		// Now we can build the Dependencies for Krita's build
		stage('Building Dependencies') {
			// This is relatively straight forward
			// We use some environment variables to tell the Krita scripts where we want them to put things
			// Then we invoke them!
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-deps.sh $HOME/appimage-workspace/ $WORKSPACE/krita/
			"""
		}

		// Lets generate the GMic Appimage
		stage('Generating GMic Appimage') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-gmic-qt.sh $HOME/appimage-workspace/
				mv $HOME/appimage-workspace/gmic_krita_qt-x86_64.appimage $WORKSPACE/
			"""
		}

		// Now we capture them for use
		stage('Capturing Dependencies') {
			// First we tar all of the dependencies up...
			sh """
				cd $HOME/appimage-workspace/
				tar -cf $WORKSPACE/krita-appimage-deps.tar deps/
			"""

			// Then we ask Jenkins to capture the tar file as an artifact
			archiveArtifacts artifacts: 'krita-appimage-deps.tar, gmic_krita_qt-x86_64.appimage', onlyIfSuccessful: true
		}
	}
}
}
